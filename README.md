Будут игнорироваться:
1. Директории .terraform
2. Файлы .tfstate
3. Файлы crash.log
4. Файлы .tfvars
5. Файлы override.tf, override.tf.json, также которые оканчиваются на _override.tf и _override.tf.json
6. Файлы .terraformrc и terraform.rc